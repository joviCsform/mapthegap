import 'package:flutter/material.dart';
import 'routes/Routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final homeRoute = Routes.activities;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        hintColor: Colors.white,
        textSelectionColor: Color.fromRGBO(255, 255, 255, 0.6),
      ),
      home: homeRoute,
    );
  }
}
