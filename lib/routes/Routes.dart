import '../pages/home.dart';
import '../pages/login_register/login.dart';
import '../pages/login_register/register.dart';
import '../pages/login_register/forgot_password.dart';
import '../pages/activities.dart';
import '../pages/issues.dart';
import '../pages/profile.dart';
import '../pages/activity_detail.dart';
import '../pages/issue_detail.dart';

class Routes {
  static final layout          = Layout();
  static final login           = Login();
  static final register        = Register();
  static final forgotPassword  = ForgotPassword();
  static final activities      = Activities();
  static final issues          = Issues();
  static final profile         = Profile();
  static final activity_detail = ActivityDetail();
  static final issue_detail    = IssueDetail();
}