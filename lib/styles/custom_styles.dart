import 'package:flutter/material.dart';

class CustomStyles {
  static final backgroundGradient = LinearGradient(
      colors: [
        Color(0xff1FF2F9),
        Color(0xff7EFFC3)
      ],
      begin: Alignment(0, 1),
      end: Alignment(0, 0)
  );

  static final backgroundColor = Color(0xfff1f1f1);
}