import 'package:flutter/material.dart';
import '../home.dart';
import '../../common_components/custom_input_field.dart';
import '../../common_components/separator.dart';
import '../../common_components/form_button.dart';
import '../../common_components/form_footer_link.dart';
import '../../routes/Routes.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Layout(
      appBar: false,
      content: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 60.0),
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/logo/app_logo.png",
                height: 120.0,
              ),
              Separator(
                height: 50.0,
              ),
              CustomInputField(
                hintText: 'Username',
              ),
              CustomInputField(
                hintText: 'Email',
              ),
              CustomInputField(
                hintText: 'Password',
              ),
              CustomInputField(
                hintText: 'Confirm Password',
              ),
              CustomInputField(
                hintText: 'Municipality',
              ),
              FormButton(
                  text: 'REGISTER'
              ),
              Separator(height: 15.0,),
              FormFooterLink(
                text: 'ALREADY HAVE AND ACCOUNT?',
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Routes.login)
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}