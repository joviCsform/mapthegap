import 'package:flutter/material.dart';
import '../home.dart';
import '../../styles/custom_styles.dart';
import '../../common_components/custom_input_field.dart';
import '../../common_components/form_button.dart';
import '../../common_components/separator.dart';
import '../../common_components/form_footer_link.dart';
import '../../routes/Routes.dart';

class ForgotPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Layout(
        appBar: false,
        content: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 100.0),
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/logo/app_logo.png",
                  height: 120.0,
                ),
                Separator(
                  height: 100.0,
                ),
                CustomInputField(
                  hintText: 'Email',
                ),
                FormButton(
                    text: 'RESET'
                ),
                Separator(height: 15.0,),
                FormFooterLink(
                  text: 'DON\'T HAVE AN ACCOUNT?',
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Routes.register)
                  ),
                ),
                Separator(height: 15.0,),
                FormFooterLink(
                  text: 'ALREADY HAVE AN ACCOUNT?',
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Routes.login)
                  ),
                )
              ],
            ),
          ),
        )
    );
  }
}