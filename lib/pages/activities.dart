import 'package:flutter/material.dart';
import 'home.dart';
import '../styles/custom_styles.dart';
import '../common_components/activity_components/activity_card.dart';

class Activities extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Layout(
      title: 'Activities and Initiatives',
      content: Container(
        height: MediaQuery.of(context).size.height,
        color: CustomStyles.backgroundColor,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ActivityCard(
                title: "Lets Paint!",
                description: "Park in our neighbourhood is full of inappropriate graffiti. Let’s paint it over together.",
                image: 'assets/avatars/lets_paint.png',
                rating: 'assets/images/five_stars.png',
              ),
              ActivityCard(
                title: "Participation course",
                description: "How to get youth to participate on your events",
                image: 'assets/avatars/learning_course.png',
                rating: 'assets/images/four_stars.png',
              ),
              ActivityCard(
                title: "ETI and young people working together for our nature!",
                description: "Let’s clean up Kentpark, one of the most important beauties of our cities.",
                image: 'assets/avatars/ETI_and_young.png',
                rating: 'assets/images/three_stars.png',
              ),
              ActivityCard(
                title: "We are creating a youth council.",
                description: "Come and take part in decision making",
                image: 'assets/avatars/youth_council.png',
                rating: 'assets/images/three_stars.png',
              ),
            ],
          ),
        ),
      ),
    );
  }
}