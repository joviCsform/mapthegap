import 'package:flutter/material.dart';
import 'home.dart';
import '../styles/custom_styles.dart';
import '../common_components/issues_components/issue_card.dart';

class Issues extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Layout(
      title: 'Local Issues',
      content: Container(
        height: MediaQuery.of(context).size.height,
        color: CustomStyles.backgroundColor,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              IssueCard(
                issue_description: "This bench in our local park has been broken for months, waiting to be fixed",
                upvotes: 321,
                downvotes: 45,
                image: 'assets/avatars/bench.png',
              ),
              IssueCard(
                issue_description: "Too many street dogs. Can they be taken to the shelter?",
                upvotes: 251,
                downvotes: 42,
                image: 'assets/avatars/dogs.png',
              ),
              IssueCard(
                issue_description: "Water ATM is not functioning for a week now",
                upvotes: 156,
                downvotes: 4,
                image: 'assets/avatars/water_atm.png',
              ),
            ],
          ),
        ),
      ),
    );
  }
}