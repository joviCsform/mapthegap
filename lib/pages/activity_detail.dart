import 'package:flutter/material.dart';
import 'home.dart';
import '../styles/custom_styles.dart';
import '../common_components/separator.dart';
import '../common_components/form_button.dart';

class ActivityDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Layout(
      title: "Lets Paint",
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () =>
          Navigator.pop(context, false)
      ),
      content: Container(
        color: CustomStyles.backgroundColor,
        child: Column(
          children: <Widget>[
            Container(
              height: 250.0,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                'assets/avatars/lets_paint.png',
                fit: BoxFit.cover,
              ),
            ),
            Separator(height: 16.0,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Image.asset(
                  'assets/images/four_stars.png',
                  height: 16.0,
                ),
              ),
            ),
            Separator(height: 16.0,),
            Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.5)
                    ),
                  ),
                  Separator(height: 25.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Organisator:',
                        style: TextStyle(
                          color: Color(0xff1D9EA1),
                          fontSize: 18.0
                        ),
                      ),
                      Text(
                        'John Smith',
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            fontSize: 16.0
                        ),
                      )
                    ],
                  ),
                  Separator(height: 25.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Date:',
                        style: TextStyle(
                            color: Color(0xff1D9EA1),
                            fontSize: 18.0
                        ),
                      ),
                      Text(
                        '25.08.2019',
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            fontSize: 16.0
                        ),
                      ),
                    ],
                  ),
                  Separator(height: 25.0),
                  FormButton(
                    text: 'APPLY',
                  )
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}