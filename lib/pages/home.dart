import 'package:flutter/material.dart';
import '../styles/custom_styles.dart';
import '../common_components/nav_link.dart';
import '../routes/Routes.dart';

class Layout extends StatelessWidget {

  final content;
  final bool appBar;
  final String title;
  final bool padding;
  final leading;

  Layout({
    this.content,
    this.appBar = true,
    this.title,
    this.padding = true,
    this.leading
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar ? AppBar(
        leading: leading != null ? leading : null,
        elevation: 0.0,
        backgroundColor: Color(0xff28F1F0),
        title: Text(
          title,
        ),
      ) : null,
      drawer: Drawer(
        child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 10.0,
                      bottom: 10.0,
                      left: 10.0,
                      right: 10.0
                  ),
                  decoration: BoxDecoration(
                      gradient:  CustomStyles.backgroundGradient
                  ),
                  height: MediaQuery.of(context).size.height * 0.29,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 80.0,
                        width: 80.0,
                        child: CircleAvatar(
                          backgroundImage: AssetImage(
                              'assets/avatars/avatar_1.png'
                          ),
                        ),
                      ),
                      Text(
                        'John Smith',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24.0,
                            fontWeight: FontWeight.w300
                        ),
                      ),
                      Text(
                        'Norfolk, Turkey',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w300
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      color: CustomStyles.backgroundColor,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: ListView(
                              padding: EdgeInsets.all(0.0),
                              children: [
                                NavLink(
                                  text: 'Activities / Initiatives',
                                  icon: Icons.share,
                                  route: Routes.activities,
                                ),
                                NavLink(
                                  text: 'Local Issues',
                                  icon: Icons.my_location,
                                  route: Routes.issues,
                                ),
                                NavLink(
                                  text: 'My Profile',
                                  icon: Icons.face,
                                  route: Routes.profile,
                                ),
                                NavLink(
                                  text: 'Log In / Log Out',
                                  icon: Icons.exit_to_app,
                                  route: Routes.login,
                                )
                              ]
                            ),
                          )
                        ],
                      ),
                    )
                )
              ],
            )
        ),
      ),
      backgroundColor: Colors.transparent,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: CustomStyles.backgroundGradient
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
              width: MediaQuery.of(context).size.width,
              padding: !appBar ? EdgeInsets.only(
                top: padding ? 16.0 : 0.0
              ) : null,
              child: content
          ),
        ),
      ),
    );
  }
}