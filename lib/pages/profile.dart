import 'package:flutter/material.dart';
import 'home.dart';
import '../styles/custom_styles.dart';
import '../common_components/separator.dart';
import '../common_components/activity_components/activity_card.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Layout(
      padding: false,
      appBar: false,
      content: Container(
        height: MediaQuery.of(context).size.height,
        color: CustomStyles.backgroundColor,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              leading: Icon(Icons.dehaze, color: Colors.white,),
              backgroundColor: Colors.transparent,
                pinned: true,
                expandedHeight: 280.0,
                flexibleSpace: Container(
                  child: FlexibleSpaceBar(
                      background: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/avatars/avatar_1.png'),
                                fit: BoxFit.cover,
                            )
                        ),
                      )
                  ),
                )
            ),
            SliverList(
                delegate: SliverChildListDelegate(
                    [
                      Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'John Smith',
                                      style: TextStyle(
                                          color: Color(0xff353A50),
                                          fontSize: 22.0
                                      ),
                                    ),
                                    Text(
                                      'Norfolk, Turkey',
                                      style: TextStyle(
                                          color: Color(0xff353A50),
                                          fontSize: 12.0
                                      ),
                                    ),
                                    Separator(height: 20.0),
                                    Text(
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                                      style: TextStyle(
                                          color: Color(0xff353A50),
                                          fontSize: 14.0
                                      ),
                                    ),
                                    Separator(height: 40.0),
                                    Text(
                                      'Past Activities:',
                                      style: TextStyle(
                                          color: Color(0xff212121),
                                          fontSize: 18.0
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Separator(height: 10.0),
                              ActivityCard(
                                title: "Let's Paint!",
                                description: "Park in our neighbourhood is full of inappropriate graffiti. Let’s paint it over together.",
                                image: 'assets/avatars/lets_paint.png',
                                rating: 'assets/images/five_stars.png',
                              ),
                              ActivityCard(
                                title: "Participation course",
                                description: "How to get youth to participate on your events",
                                image: 'assets/avatars/learning_course.png',
                                rating: 'assets/images/four_stars.png',
                              ),
                              ActivityCard(
                                title: "ETI and young people working together for our nature!",
                                description: "Let’s clean up Kentpark, one of the most important beauties of our cities.",
                                image: 'assets/avatars/ETI_and_young.png',
                                rating: 'assets/images/three_stars.png',
                              ),
                              ActivityCard(
                                title: "We are creating a youth council.",
                                description: "Come and take part in decision making",
                                image: 'assets/avatars/youth_council.png',
                                rating: 'assets/images/three_stars.png',
                              ),
                            ],
                          )
                      )
                    ]
                )
            ),
          ],
        ),
      ),
    );
  }
}