import 'package:flutter/material.dart';

class CustomInputField extends StatelessWidget {

  final String hintText;
  final double margin;

  CustomInputField({
    this.hintText,
    this.margin = 5.0
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: margin),
      child: TextFormField(
        cursorColor: Colors.white,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
            borderSide: BorderSide(
              color: Colors.white,
              width: 1.0
            )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
              borderSide: BorderSide(
                  color: Colors.white,
                  width: 1.0
              )
          ),
          filled: false,
          labelText: hintText,
          labelStyle: TextStyle(
            color: Color.fromRGBO(0, 0, 0, 0.5)
          ),
          fillColor: Colors.white70),
      ),
    );
  }
}