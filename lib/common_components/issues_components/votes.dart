import 'package:flutter/material.dart';
import '../../common_components/separator.dart';

class Votes extends StatelessWidget {
  
  final bool vote_type;
  final int positive;
  final int negative;
  
  Votes({
    this.vote_type,
    this.positive,
    this.negative
  });
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.0,
      width: 120.0,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(20.0),
        border: Border.all(
          color: vote_type ?
          Color(0xff1D9EA1) :
          Color(0xffE65267)
        ),
      ),
      child: Center(
        child: vote_type ?
        Text(
          'Supporting: ${positive}',
          style: TextStyle(
            color: Color(0xff1D9EA1),
            fontSize: 12.0,
            fontWeight: FontWeight.w300
          ),
        ) : Text(
          'Not Supporting: ${negative}',
          style: TextStyle(
            color: Color(0xffE65267),
            fontSize: 12.0,
            fontWeight: FontWeight.w300
          ),
        )
      ),
    );
  }
}