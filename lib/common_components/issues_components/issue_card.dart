import 'package:flutter/material.dart';
import '../../common_components/separator.dart';
import 'votes.dart';
import '../../routes/Routes.dart';

class IssueCard extends StatelessWidget {

  final String issue_description;
  final int upvotes;
  final int downvotes;
  final image;

  IssueCard({
    this.issue_description,
    this.downvotes,
    this.upvotes,
    this.image
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Routes.issue_detail)
      ),
      child: Container(
        margin: EdgeInsets.only(bottom: 10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.2),
                offset: Offset(0, 2),
                blurRadius: 4
              )
            ]
        ),
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                image,
                height: 200.0,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      issue_description,
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Color(0xff1D9EA1)
                      ),
                    ),
                  ),
                  Separator(height: 10.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Votes(
                        vote_type: true,
                        positive: upvotes,
                      ),
                      Votes(
                        vote_type: false,
                        negative: downvotes,
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}