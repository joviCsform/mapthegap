import 'package:flutter/material.dart';

class Separator extends StatelessWidget {
  final double height;
  final double width;

  Separator({
    this.width,
    this.height
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
    );
  }
}