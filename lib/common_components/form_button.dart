import 'package:flutter/material.dart';

class FormButton extends StatelessWidget {

  final String text;
  final double margin;

  FormButton({
    this.text,
    this.margin = 5.0
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(vertical: margin),
      height: 60.0,
      child: RaisedButton(
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        onPressed: () {},
        child: Text(
          text,
          style: TextStyle(
            color: Color(0xff1D9EA1),
            fontSize: 20.0,
            fontWeight: FontWeight.w300
          ),
        ),
      )
    );
  }
}