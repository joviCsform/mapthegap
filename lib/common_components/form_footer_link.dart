import 'package:flutter/material.dart';

class FormFooterLink extends StatelessWidget {

  final String text;
  final onTap;

  FormFooterLink({
    this.text,
    this.onTap
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 15.0,
            fontWeight: FontWeight.w500
          ),
        ),
      ),
    );
  }
}