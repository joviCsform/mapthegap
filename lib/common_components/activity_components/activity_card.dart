import 'package:flutter/material.dart';
import '../../common_components/separator.dart';
import '../../routes/Routes.dart';

class ActivityCard extends StatelessWidget {

  final String title;
  final image;
  final String description;
  final rating;

  ActivityCard({
    this.title,
    this.image,
    this.description,
    this.rating
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Routes.activity_detail)
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.2),
              offset: Offset(0, 2),
              blurRadius: 4
            )
          ]
        ),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                image,
                height: 200.0,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          title,
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Color(0xff1D9EA1),
                          ),
                        ),
                      ),
                      Image.asset(
                        rating,
                        height: 16.0,
                      )
                    ],
                  ),
                  Separator(height: 10.0),
                  Container(
                    child: Text(
                      description,
                      style: TextStyle(
                        fontSize: 14.0,
                        color: Color.fromRGBO(0, 0, 0, 0.4)
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}