import 'package:flutter/material.dart';

class NavLink extends StatelessWidget {

  final route;
  final text;
  final icon;

  NavLink({
    this.route,
    this.text,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                offset: Offset(0, 2),
                blurRadius: 4.0
            )
          ]
      ),
      child: ListTile(
        leading: Icon(
          icon,
          color: Color(0xff1D9EA1),
        ),
        title: Text(
          text,
          style: TextStyle(
              color: Color(0xff1D9EA1),
              fontSize: 16.0
          ),
        ),
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => route),
          );
        },
      ),
    );
  }
}